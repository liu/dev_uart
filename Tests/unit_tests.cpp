//
// Created by liuwuhao on 17.12.21.
//
#include "gtest/gtest.h"
#include "comminucation/Frame.h"
#include "comminucation/DebugWrapper.h"
#include "comminucation/Connection.h"
#include "comminucation/Command.h"
#include "comminucation/PUFTest.h"

namespace {
    vector<uint8_t> raw = {85, 4, 67, 68, 69, 70, 205, 199, 82, 54, 170};
    vector<uint8_t> data = {67, 68, 69, 70};
    vector<uint8_t> raw1 = {85};
    vector<uint8_t> raw2 = {85, 4, 67, 68, 69, 70, 203, 199, 82, 54, 170}; // crc_error
    vector<uint8_t> raw3 = {85, 1, 0, 210, 2, 239, 141, 170};

    // "TEST{id: 10}"
    vector<uint8_t> test_data = {84, 69, 83, 84, 123, 105, 100, 58, 32, 49, 48, 125};
    vector<uint8_t> test_payload = {123, 105, 100, 58, 32, 49, 48, 125};
    vector<uint8_t> test_frame = {85, 12, 84, 69, 83, 84, 123, 105, 100, 58, 32, 49, 48, 125, 125, 163, 77, 240, 170};
    // "STATE?"
    vector<uint8_t> state_data = {83, 84, 65, 84, 69, 63};
    vector<uint8_t> state_frame = {85, 6, 83, 84, 65, 84, 69, 63, 133, 62, 157, 184, 170};
    // "RESULT?"
    vector<uint8_t> result_data = {82, 69, 83, 85, 76, 84, 63};
    vector<uint8_t> result_frame = {85, 7, 82, 69, 83, 85, 76, 84, 63, 228, 226, 55, 125, 170};


    TEST(Frame, BASIC) {
      Frame f(raw);
      EXPECT_EQ(f.length, 4);
      EXPECT_EQ(f.data, data);
      EXPECT_THROW(Frame f1(raw1), runtime_error);
      EXPECT_THROW(Frame f2(raw2), runtime_error);
      Frame f3(raw3);
      EXPECT_EQ(f3.data, vector<uint8_t>{0});
    }

    TEST(Frame, build_Frame) {
      Frame f = Frame::buildFrame(data);
      EXPECT_EQ(f.length, 4);
      EXPECT_EQ(f.data, data);
      EXPECT_EQ(f.raw, raw);

      Frame f1 = Frame::buildFrame(raw1);
      EXPECT_EQ(f1.length, 1);
      EXPECT_EQ(f1.data, raw1);
    }

    TEST(DebugWrapper, BASIC) {
      DebugWrapper d1(raw);
      EXPECT_EQ(d1.ReceiveToIdle(0, 0), raw);
      EXPECT_EQ(d1.ReceiveData(0, 0), raw);
      d1.msg = raw2;
      EXPECT_EQ(d1.ReceiveData(0, 0), raw2);
    }

    TEST(Connection, BASIC) {
      DebugWrapper d1(raw);
      Connection c1(d1);
      Frame *f1 = c1.ReceiveFrame();
      EXPECT_EQ(f1->data, data);
      d1.msg = raw2;
      EXPECT_THROW(c1.ReceiveFrame(), runtime_error);
    }

    TEST(Command, BASIC) {
      Command c(test_data);
      EXPECT_EQ(c.command, TEST_COMMAND);
      EXPECT_EQ(c.payload, test_payload);
      Command c1(state_data);
      EXPECT_EQ(c1.command, STATE_COMMAND);
      Command c2(result_data);
      EXPECT_EQ(c2.command, RESULT_COMMAND);
      EXPECT_THROW(Command c3(raw), runtime_error);
    }

    TEST(Command, COMPLETE) {
      DebugWrapper d(test_frame);
      Connection c(d);
      Frame *f = c.ReceiveFrame();
      Command com(f->data);
      EXPECT_EQ(com.command, TEST_COMMAND);
      EXPECT_EQ(com.payload, test_payload);

      d.msg = state_frame;
      f = c.ReceiveFrame();
      com = Command(f->data);
      EXPECT_EQ(com.command, STATE_COMMAND);

      d.msg = result_frame;
      f = c.ReceiveFrame();
      com = Command(f->data);
      EXPECT_EQ(com.command, RESULT_COMMAND);

      d.msg = raw;
      f = c.ReceiveFrame();
      EXPECT_THROW(Command(f->data), runtime_error);
    }

    TEST(PUFTest, BASIC) {
      DebugWrapper d(test_frame);
      Connection c(d);
      PUFTest pt(c);
      Command *com = pt.ReceiveCommand();
      EXPECT_EQ(com->command, TEST_COMMAND);
      EXPECT_EQ(com->payload, test_payload);

      d.msg = result_frame;
      Connection c1(d);
      com = pt.ReceiveCommand();
      EXPECT_EQ(com->command, RESULT_COMMAND);
    }
}
