//
// Created by liuwuhao on 20.12.21.
//

#include "PUFTest.h"
#include "stm32f4xx_hal.h"


Command* PUFTest::ReceiveCommand() {
  Frame *frame = connection.ReceiveFrame();
  Command *c = new Command(frame->data);
  return c;
}

void PUFTest::run() {
  Command *c;
  while(1) {
    try {
      c = ReceiveCommand();
    } catch (runtime_error e) {
      cout << e.what() << endl;
      continue;
    }
    cout << "received command: " << c->command << endl;
    if (c->command == STATE_COMMAND) {
      sendState();
    } else if (c->command == RESULT_COMMAND) {
      sendResult();
    } else if (c->command == TEST_COMMAND) {
      executeTest(c->payload);
    }
  }
}

void PUFTest::sendState() {
  vector<uint8_t> msg = {state};
  connection.TransmitFrame(msg);
}

void PUFTest::sendResult() {
  if (state != 2) {
    cout << "no result can be sent." << endl;
  }
  vector<uint8_t> msg = {result};
  connection.TransmitFrame(msg);
  state = 0;
}

void PUFTest::executeTest(vector<uint8_t> test) {
  state = 1;
  cout << "starting test:";
  HAL_Delay(5000);
  for (char ch : test) {
    cout << ch;
  }
  cout << endl;
  state = 2;
}
