//
// Created by liuwuhao on 17.12.21.
//
#include "iostream"
#include "boost/crc.hpp"
#include "Frame.h"

Frame::Frame(vector<uint8_t> raw) : raw(raw) {
  if (raw.front() != 0x55 || raw.back() != 0xaa) {
    cout << "error: received: [";
    for (int b : raw) {
      cout << b << " ";
    }
    cout << "]" << endl;
    throw runtime_error("invalid frame.");
  } else {
    length = raw.at(1);
    data = {raw.begin() + 2, raw.begin() + 2 + length};

    uint8_t crc_data[4];
    copy(raw.end() - 5, raw.end() - 1, crc_data);
    uint32_t crc_received =
            crc_data[0] << 24 |
            crc_data[1] << 16 |
            crc_data[2] << 8 |
            crc_data[3];

    boost::crc_32_type crc_computed;
    uint8_t data_arr[length];
    copy(this->data.begin(), this->data.end(), data_arr);
    crc_computed.process_bytes(data_arr, length);
    if (crc_received != crc_computed.checksum()) {
      for (uint8_t value : data_arr) {
        cout << value << " ";
      }
      cout << endl;
      throw runtime_error("crc error.");
    }
  }
}

/**
 * @brief create a valid Frame
 * */
Frame Frame::buildFrame(vector<uint8_t> data) {
  vector<uint8_t> raw_frame;
  uint16_t length = data.size();
  raw_frame.push_back(0x55);
  raw_frame.push_back(data.size());
  raw_frame.insert(raw_frame.end(), data.begin(), data.end());
  boost::crc_32_type crc_computed;
  uint8_t data_arr[length];
  copy(data.begin(), data.end(), data_arr);
  crc_computed.process_bytes(data_arr, length);
  uint32_t checksum = crc_computed.checksum();
  raw_frame.push_back((checksum >> 24) & 0xff);
  raw_frame.push_back((checksum >> 16) & 0xff);
  raw_frame.push_back((checksum >> 8) & 0xff);
  raw_frame.push_back(checksum & 0xff);
  raw_frame.push_back(0xaa);
  return Frame(raw_frame);
}

