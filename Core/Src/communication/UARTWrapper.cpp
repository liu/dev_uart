#ifndef DEV_UART_UARTWRAPPER_H
#define DEV_UART_UARTWRAPPER_H

#include "vector"
#include "UARTWrapper.h"

void UARTWrapper::SendData(vector<uint8_t> msg, uint32_t timeout) {
  // TODO: timeout
  uint8_t *p_msg = &msg[0];
  HAL_UART_Transmit(&interface, p_msg, msg.size(), timeout);
}

vector<uint8_t> UARTWrapper::ReceiveData(uint16_t size, uint32_t timeout) {
  // TODO: timeout
  uint8_t data[size];
  HAL_UART_Receive(&interface, data, size, timeout);
  vector<uint8_t> r(data, data + size);
  return r;
}

vector<uint8_t> UARTWrapper::ReceiveToIdle(uint16_t size, uint32_t timeout) {
  // TODO: timeout
  uint8_t data[size];
  uint16_t real_size;
  HAL_UARTEx_ReceiveToIdle(&interface, data, size, &real_size, timeout);
  vector<uint8_t> r(data, data + real_size);
  return r;
}

#endif

