//
// Created by liuwuhao on 20.12.21.
//
#include <cstring>
#include "Command.h"

Command::Command(vector<uint8_t> raw) {
  if (raw.size() < 4) {
    throw runtime_error("invalid command.");
  }
  string str(raw.begin(), raw.begin() + strlen(TEST_COMMAND));
  if (str == TEST_COMMAND) {
    command = TEST_COMMAND;
    payload = {raw.begin() + 4, raw.end()};
    return;
  }
  str = string(raw.begin(), raw.begin() + strlen(STATE_COMMAND));
  if (str == STATE_COMMAND) {
    command = STATE_COMMAND;
    return;
  }
  str = string(raw.begin(), raw.begin() + strlen(RESULT_COMMAND));
  if (str == RESULT_COMMAND) {
    command = RESULT_COMMAND;
    return;
  }
  throw runtime_error("invalid command.");
//  if (string comm)
}

