//
// Created by liuwuhao on 16.12.21.
//
#include "iostream"
#include "exception"
#include "Connection.h"

Frame* Connection::ReceiveFrame() {
  vector<uint8_t> raw = wrapper.ReceiveToIdle(MAX_FRAME_SIZE, RX_TIMEOUT);
  Frame *f = new Frame(raw);
  return f;
}

int Connection::TransmitFrame(vector<uint8_t> data) {
  // TODO: timeout
  Frame f = Frame::buildFrame(data);
  wrapper.SendData(f.raw, TX_TIMEOUT);
  return 0;
}
