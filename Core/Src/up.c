#include <stdint.h>
#include <string.h>
#include <malloc.h>

#include "stdio.h"

/**
 * generate a up frame
 * */
void gen_frame(uint8_t* s_buf, uint8_t s_size, uint8_t** d_buf, uint8_t* d_size) {
  uint8_t len = 2 + 1 + s_size;
  uint8_t buffer[len];
  buffer[0] = 0x55;
  buffer[len - 1] = 0xaa;
  buffer[1] = s_size;
  memcpy(&buffer[2], s_buf, s_size);

  *d_size = len;
  *d_buf = buffer;
}