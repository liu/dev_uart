#ifndef DEV_UART_INTERFACEWRAPPER_H
#define DEV_UART_INTERFACEWRAPPER_H

#include <vector>
#include <cstdint>

using namespace std;

class InterfaceWrapper {
public:
    virtual void SendData(vector<uint8_t> msg, uint32_t timeout) = 0;
    virtual vector<uint8_t> ReceiveData(uint16_t size, uint32_t timeout) = 0;
    virtual vector<uint8_t> ReceiveToIdle(uint16_t size, uint32_t timeout) = 0;
};

#endif

