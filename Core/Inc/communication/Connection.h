//
// Created by liuwuhao on 16.12.21.
//

#ifndef DEV_UART_CONNECTION_H
#define DEV_UART_CONNECTION_H

#define MAX_FRAME_SIZE 256 // max length of a frame
#define RX_TIMEOUT 10000 // receive timeout
#define TX_TIMEOUT 10000 // transimit timeout

#include "cstdint"
#include "vector"
#include "InterfaceWrapper.h"
#include "Frame.h"

using namespace std;

class Connection {
private:
    InterfaceWrapper &wrapper;
public:
    // TODO: send ack
    Connection(InterfaceWrapper &wrapper) : wrapper(wrapper) {}
    Frame* ReceiveFrame();
    int TransmitFrame(vector<uint8_t> data);
};
#endif //DEV_UART_CONNECTION_H
