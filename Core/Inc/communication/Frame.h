//
// Created by liuwuhao on 17.12.21.
//

#ifndef DEV_UART_FRAME_H
#define DEV_UART_FRAME_H
#include "cstdint"
#include "vector"

using namespace std;
/**
+--------------+----------------+--------------------+----------------+--------------+
| 0x55(1 byte) | length(1 byte) | data(length bytes) | crc32(4 bytes) | 0xaa(1 byte) |
+--------------+----------------+--------------------+----------------+--------------+
 * A frame is like above.
 * */
 class Frame {
 public:
     vector<uint8_t> raw;
     uint16_t length;
     vector<uint8_t> data;

     Frame(vector<uint8_t> raw);

     static Frame buildFrame(vector<uint8_t> raw);
 };

#endif //DEV_UART_FRAME_H
