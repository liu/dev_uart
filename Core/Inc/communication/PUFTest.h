//
// Created by liuwuhao on 20.12.21.
//

#ifndef DEV_UART_PUFTEST_H
#define DEV_UART_PUFTEST_H

#include "Command.h"
#include "Connection.h"

class PUFTest {
private:
    Connection &connection;
public:
    PUFTest(Connection &connection) : connection(connection) {}

    Command* ReceiveCommand();

    // 0: idle
    // 1: busy
    // 2: finished(result is available)
    int state = 0;

    // fake data
    // specific to the kind of test
    int result = 20;

    void sendState();

    void sendResult();

    void executeTest(vector<uint8_t>);

    void run();
};
#endif //DEV_UART_PUFTEST_H
