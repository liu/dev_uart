//
// Created by liuwuhao on 20.12.21.
//

#ifndef DEV_UART_COMMAND_H
#define DEV_UART_COMMAND_H

#include "iostream"
#include "vector"
#include "cstdint"

using namespace std;

#define TEST_COMMAND "TEST"
#define STATE_COMMAND "STATE?"
#define RESULT_COMMAND "RESULT?"

class Command {
public:
    string command;
    vector<uint8_t> payload;
    Command(vector<uint8_t> raw);
};
#endif //DEV_UART_COMMAND_H
