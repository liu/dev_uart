//
// Created by liuwuhao on 20.12.21.
//

#ifndef DEV_UART_DEBUGWRAPPER_H
#define DEV_UART_DEBUGWRAPPER_H

#include <cstdint>
#include "InterfaceWrapper.h"

/**
 * @brief This is a Wrapper class for debug, the received data can be defined as required
 * */
class DebugWrapper : public InterfaceWrapper {
public:
    vector<uint8_t> msg;
    DebugWrapper(vector<uint8_t> msg) : msg(msg) {}
    void SendData(vector<uint8_t> msg, uint32_t timeout);
    vector<uint8_t> ReceiveData(uint16_t size, uint32_t timeout);
    vector<uint8_t> ReceiveToIdle(uint16_t size, uint32_t timeout);
};

#endif //DEV_UART_DEBUGWRAPPER_H
