#ifndef DEV_UART_BUFFER_H
#define DEV_UART_BUFFER_H


#include "InterfaceWrapper.h"

class Buffer {
private:
    InterfaceWrapper *buffer;
public:
    Buffer(const InterfaceWrapper &buffer);
};

#endif //DEV_UART_BUFFER_H