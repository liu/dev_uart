#include <cstdint>
#include "InterfaceWrapper.h"
#include "stm32f4xx_hal.h"

class UARTWrapper : public InterfaceWrapper {
private:
UART_HandleTypeDef &interface;
public:
    UARTWrapper(UART_HandleTypeDef &uart) : interface(uart) {};
    void SendData(vector<uint8_t> msg, uint32_t timeout);
    vector<uint8_t> ReceiveData(uint16_t size, uint32_t timeout);
    vector<uint8_t> ReceiveToIdle(uint16_t size, uint32_t timeout);
};